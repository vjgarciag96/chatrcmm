package vjgarciag96.chatrcmm.data.remote;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Query;
import vjgarciag96.chatrcmm.data.dto.GeolocationBody;
import vjgarciag96.chatrcmm.data.dto.GeolocationResponse;

public interface GeolocationAPIService {
    @POST("geolocate")
    Call<GeolocationResponse> getLocation(@Query("key") String apiKey, @Body GeolocationBody body);
}
