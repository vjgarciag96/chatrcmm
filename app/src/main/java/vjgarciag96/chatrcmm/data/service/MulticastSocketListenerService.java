package vjgarciag96.chatrcmm.data.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.SparseArray;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import vjgarciag96.chatrcmm.common.AppExecutors;
import vjgarciag96.chatrcmm.common.BundleKeys;
import vjgarciag96.chatrcmm.common.MessageType;
import vjgarciag96.chatrcmm.data.event.MessageEvent;
import vjgarciag96.chatrcmm.data.model.Message;

/**
 * Created by victor on 12/03/18.
 */

public class MulticastSocketListenerService extends Service {

    private static final String TAG = "MulticastSocketListener";
    private static final int MAX_BUFFER = 65535;
    private static final int DEFAULT_LISTENING_PORT = 4446;

    private WifiManager.MulticastLock multicastLock = null;

    private String groupAddress = "";
    private int port = -1;

    private MulticastSocket socket;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            groupAddress = intent.getStringExtra(BundleKeys.MULTICAST_ADDRESS_KEY);
            port = intent.getIntExtra(BundleKeys.MULTICAST_PORT_KEY, DEFAULT_LISTENING_PORT);
            if (groupAddress != null && !TextUtils.isEmpty(groupAddress))
                AppExecutors.getInstance().multicastThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        startMulticastSocketListenerService(groupAddress, port);
                    }
                });
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        try {
            socket.leaveGroup(InetAddress.getByName(groupAddress));
            if (socket != null)
                socket.close();
            Log.d(TAG, "Shutting down...");
        } catch (IOException e) {
            e.printStackTrace();
        }
        ((AppExecutors.MulticastThreadExecutor) AppExecutors.getInstance().multicastThread()).shutdown();
        super.onDestroy();
    }

    public void init() {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        multicastLock = wifiManager.createMulticastLock("multicast-group");
        if (multicastLock != null)
            multicastLock.acquire();
    }

    public void uninit() {
        if (multicastLock != null) {
            multicastLock.release();
            multicastLock = null;
        }
    }

    public void startMulticastSocketListenerService(String groupIp, int port) {
        try {
            while (true) {
                init();
                InetAddress group = InetAddress.getByName(groupIp);
                socket = new MulticastSocket(port);
                socket.joinGroup(group);
                socket.setLoopbackMode(false);
                socket.setSoTimeout(0);
                byte[] buffer = new byte[MAX_BUFFER];
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                socket.receive(packet);
                String message = new String(buffer, StandardCharsets.UTF_8);
                int messageCode = MessageType.MESSAGE_TYPE_IMAGE;
                if (message.substring(0, 1).equals(String.valueOf(MessageType.MESSAGE_TYPE_TEXT))) {
                    messageCode = MessageType.MESSAGE_TYPE_TEXT;
                    message = message.substring(1);
                    message = message.replaceAll("\0+$", "");
                    EventBus.getDefault().postSticky(new MessageEvent(messageCode,
                            message.getBytes(), packet.getAddress().getHostAddress()));
                } else {
                    String packetsSizeMessage = message.substring(2, message.length() - 1);
                    packetsSizeMessage = packetsSizeMessage.replaceAll("\0+$", "");
                    packetsSizeMessage = packetsSizeMessage.substring(0, packetsSizeMessage.length() - 1);
                    Log.d(TAG, packetsSizeMessage);
                    int totalBytesToReceive = Integer.valueOf(packetsSizeMessage);
                    int imageBytesReceived = 0;
                    SparseArray<byte[]> image = new SparseArray<>();
                    while (imageBytesReceived < totalBytesToReceive) {
                        Log.d(TAG, "bytes received " + imageBytesReceived + "/" + totalBytesToReceive);
                        buffer = new byte[MAX_BUFFER];
                        packet = new DatagramPacket(buffer, buffer.length);
                        socket.receive(packet);
                        String bytesCodified = new String(buffer);
                        String[] messageSplitted = bytesCodified.split("/");
                        int headerLength = (messageSplitted[0] + "/").getBytes().length;
                        int dataSize = buffer.length - headerLength;
                        if(dataSize > totalBytesToReceive - imageBytesReceived)
                            dataSize = totalBytesToReceive - imageBytesReceived;
                        byte[] data = new byte[dataSize];
                        System.arraycopy(buffer, headerLength, data, 0, dataSize);
                        image.append(Integer.valueOf(messageSplitted[0]), data);
                        imageBytesReceived += dataSize;
                        Log.d(TAG, "getting packet with seqno = " + messageSplitted[0]);
                    }
                    Log.d(TAG, "terminated receiving packets "+ imageBytesReceived + "/" + totalBytesToReceive);
                    byte[] data = new byte[totalBytesToReceive];
                    int offset = 0;
                    for (int i = 0; i < image.size(); i++) {
                        int datasize = image.get(i).length;
                        if(datasize > totalBytesToReceive)
                            datasize = totalBytesToReceive;
                        System.arraycopy(image.get(i), 0, data, offset, datasize);
                        offset += datasize;
                        totalBytesToReceive -= datasize;
                    }
                    Log.d(TAG, "ImageSTR = " + new String(Base64.encode(data, 0)));
                    EventBus.getDefault().postSticky(new MessageEvent(messageCode,
                            data, packet.getAddress().getHostAddress()));
                }
                uninit();
            }
        } catch (SocketException e) {
            Log.e(TAG, "run: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "run: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "run: " + e.getMessage() + "\n"+ e.getLocalizedMessage());
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
