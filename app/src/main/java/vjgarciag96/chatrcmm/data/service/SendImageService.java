package vjgarciag96.chatrcmm.data.service;

import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.Socket;

import vjgarciag96.chatrcmm.common.AppExecutors;
import vjgarciag96.chatrcmm.common.BundleKeys;
import vjgarciag96.chatrcmm.common.SocketAddressValidator;
import vjgarciag96.chatrcmm.common.MessageType;
import vjgarciag96.chatrcmm.data.local.ChatDatabase;
import vjgarciag96.chatrcmm.data.model.Contact;
import vjgarciag96.chatrcmm.data.model.Message;

/**
 * Created by victor on 12/03/18.
 */

public class SendImageService extends Service {

    private static final String TAG = "SendImageService";

    private static final int MAX_BUFFER = 65507;
    private static final int DEFAULT_PORT = 4446;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final String imagePath = intent.getStringExtra(BundleKeys.IMAGE_PATH_KEY);
        final String targetIp = intent.getStringExtra(BundleKeys.TARGET_IP_ADDRESS_KEY);
        final int targetPort = intent.getIntExtra(BundleKeys.TARGET_PORT_KEY, DEFAULT_PORT);

        AppExecutors.getInstance().networkIO().execute(new Runnable() {
            @Override
            public void run() {
                if (SocketAddressValidator.isMulticast(targetIp))
                    sendImage(getBytesFromBitmap(getBitmapFromImagePath(imagePath, 512)), targetIp, targetPort);
                else
                    sendImage(getBytesFromBitmap(getBitmapFromImagePath(imagePath, 1024)), targetIp, targetPort);
            }
        });
        return super.onStartCommand(intent, flags, startId);
    }

    public void sendImage(byte[] bitmap, String targetIp, int port) {
        try {
            if (SocketAddressValidator.isMulticast(targetIp)) {
                Log.d(TAG, "ImageSTR = " + new String(Base64.encode(bitmap, 0)));
                InetAddress ipAddress = InetAddress.getByName(targetIp);
                MulticastSocket groupSocket = new MulticastSocket(port);
                groupSocket.joinGroup(ipAddress);
                groupSocket.setLoopbackMode(false);
                int bytesOffset = 0;
                String imageCode = "1/" + String.valueOf(bitmap.length) + "/";
                Log.i(TAG, "we're going to send " + bitmap.length + " Bytes");
                groupSocket.send(new DatagramPacket(imageCode.getBytes(), 0, imageCode.getBytes().length, ipAddress, port));
                int packetOffset = 0;
                while (bytesOffset <= bitmap.length) {
                    byte[] packet = new byte[MAX_BUFFER];
                    int dataSize = MAX_BUFFER;
                    String header = String.valueOf(packetOffset) + "/";
                    if (bitmap.length - bytesOffset < dataSize - header.getBytes().length)
                        dataSize = bitmap.length + header.getBytes().length - bytesOffset;
                    System.arraycopy(header.getBytes(), 0, packet, 0, header.getBytes().length);
                    System.arraycopy(bitmap, bytesOffset, packet, header.getBytes().length, dataSize - header.getBytes().length);
                    bytesOffset += dataSize - header.getBytes().length;
                    packetOffset++;
                    groupSocket.send(new DatagramPacket(packet, 0, packet.length, ipAddress, port));
                }
                groupSocket.leaveGroup(ipAddress);
            } else {
                Socket socket = new Socket(targetIp, 4445);
                byte[] imageBytes = bitmap;
                OutputStream outputStream = socket.getOutputStream();
                DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
                dataOutputStream.writeInt(MessageType.MESSAGE_TYPE_IMAGE);
                dataOutputStream.writeInt(imageBytes.length);
                dataOutputStream.write(imageBytes, 0, imageBytes.length);
                socket.close();
                storeImage(imageBytes, targetIp);
            }
        } catch (IOException e) {
            Log.i(TAG, e.getMessage());
        }
    }

    private void storeImage(final byte[] data, final String targetAddress) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                Contact contact = ChatDatabase.getInstance(getBaseContext()).contactDAO().getContactByIp(targetAddress);
                Message message = new Message("Yo", true, false, contact.getContactId(), MessageType.MESSAGE_TYPE_IMAGE, data);
                ChatDatabase.getInstance(getBaseContext()).messageDAO().insertMessage(message);
            }
        });
    }

    private Bitmap getBitmapFromImagePath(String imagePath, int maxSize) {
        File imgFile = new File(imagePath);
        Bitmap myBitmap = null;
        ExifInterface exif;
        if (imgFile.exists()) {
            myBitmap = getResizedBitmap(BitmapFactory.decodeFile(imgFile.getAbsolutePath()), maxSize);
            try {
                exif = new ExifInterface(imgFile.getAbsolutePath());
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                Log.d("EXIF", "Exif: " + orientation);
                Matrix matrix = new Matrix();
                if (orientation == 6) {
                    matrix.postRotate(90);
                } else if (orientation == 3) {
                    matrix.postRotate(180);
                } else if (orientation == 8) {
                    matrix.postRotate(270);
                }
                myBitmap = Bitmap.createBitmap(myBitmap, 0, 0, myBitmap.getWidth(), myBitmap.getHeight(), matrix, true); // rotating bitmap
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return myBitmap;
    }

    private byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        Log.i(TAG, "height = " + height);
        Log.i(TAG, "width = " + width);
        Log.i(TAG, "ratio = " + bitmapRatio);
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
