package vjgarciag96.chatrcmm.data.service;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

import vjgarciag96.chatrcmm.R;
import vjgarciag96.chatrcmm.common.AppExecutors;
import vjgarciag96.chatrcmm.common.BundleKeys;
import vjgarciag96.chatrcmm.common.MessageType;
import vjgarciag96.chatrcmm.data.event.MessageEvent;
import vjgarciag96.chatrcmm.data.local.ChatDatabase;
import vjgarciag96.chatrcmm.data.model.Contact;
import vjgarciag96.chatrcmm.data.model.Message;
import vjgarciag96.chatrcmm.data.model.MessageCreator;
import vjgarciag96.chatrcmm.ui.chat.ChatActivity;

/**
 * Created by victor on 24/03/18.
 */

public class MessagesPullService extends Service {

    private static final String TAG = "MessagesService";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        AppExecutors.getInstance().networkIO().execute(new Runnable() {
            @Override
            public void run() {
                startListening();
            }
        });
        return super.onStartCommand(intent, flags, startId);
    }

    private void startListening() {
        Log.i(TAG, "startListening: ");
        while (true) {
            Log.i(TAG, "startListening: iterando...");
            Socket socket = null;
            try {
                ServerSocket serverSocket = new ServerSocket(4445);
                if (socket == null)
                    socket = serverSocket.accept();
                InputStream inputStream = socket.getInputStream();
                DataInputStream dataInputStream = new DataInputStream(inputStream);
                int typeCode = dataInputStream.readInt();
                int length = dataInputStream.readInt();
                byte[] data = new byte[length];
                if (length > 0)
                    dataInputStream.readFully(data);
                notifyMessage(typeCode, data, socket.getInetAddress().getHostAddress());
                serverSocket.close();
                socket.close();
            } catch (IOException e) {
                Log.i(TAG, "startListening: " + e.getMessage());
                break;
            }
        }
    }

    private void notifyMessage(final int typeCode, final byte[] data, final String sender) {
        String message = "";
        switch (typeCode) {
            case MessageType.MESSAGE_TYPE_TEXT:
                message = "Texto";
                break;
            case MessageType.MESSAGE_TYPE_IMAGE:
                message = "Imagen";
                break;
            case MessageType.MESSAGE_TYPE_LOCATION:
                message = "Ubicación";
                break;
        }

        final String notificationText = message;
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                Contact contact = ChatDatabase.getInstance(getApplicationContext()).contactDAO().getContactByIp(sender);
                long id;
                if (contact == null) {
                    Contact newContact = new Contact("(" + sender + ")", sender, 4445);
                    id = ChatDatabase.getInstance(getApplicationContext()).contactDAO().insertContact(newContact);
                } else
                    id = contact.getContactId();
                final long contactId = id;
                final Message messageReceived;
                try {
                    messageReceived = MessageCreator.create(typeCode, data, sender, contactId);
                    AppExecutors.getInstance().diskIO().execute(new Runnable() {
                        @Override
                        public void run() {
                            ChatDatabase.getInstance(getBaseContext())
                                    .messageDAO().insertMessage(messageReceived);
                        }
                    });
                    if (EventBus.getDefault().hasSubscriberForEvent(MessageEvent.class))
                        EventBus.getDefault().post(new MessageEvent(typeCode, data, sender));
                    AppExecutors.getInstance().mainThread().execute(new Runnable() {
                        @Override
                        public void run() {
                            String rawIp = sender.replace(".", "");
                            int notificationId = Integer.valueOf(rawIp);
                            Intent chatActivityIntent = new Intent(getApplicationContext(), ChatActivity.class);
                            chatActivityIntent.putExtra(BundleKeys.CONTACT_ID_KEY, contactId);
                            PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0,
                                    chatActivityIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getBaseContext())
                                    .setSmallIcon(R.drawable.ic_message)
                                    .setContentTitle("Mensaje nuevo de " + sender)
                                    .setContentText(notificationText)
                                    .setContentIntent(contentIntent)
                                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getBaseContext());
                            notificationManager.notify(notificationId, mBuilder.build());
                        }
                    });

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });

    }
}
