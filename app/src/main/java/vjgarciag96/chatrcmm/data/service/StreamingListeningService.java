package vjgarciag96.chatrcmm.data.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executor;

import vjgarciag96.chatrcmm.common.AppExecutors;
import vjgarciag96.chatrcmm.common.BundleKeys;
import vjgarciag96.chatrcmm.common.MessageType;
import vjgarciag96.chatrcmm.data.event.StreamingOkEvent;
import vjgarciag96.chatrcmm.data.event.StreamingRequestEvent;

/**
 * Created by victor on 2/04/18.
 */

public class StreamingListeningService extends Service {

    private static final String TAG = "StreamingListening";
    private static final int LISTENING_PORT = 4448;

    private String senderIp = "";
    private Executor multicastThreadExecutor;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        senderIp = intent.getExtras().getString(BundleKeys.SENDER_IP_ADDRESS_KEY);
        multicastThreadExecutor = AppExecutors.getInstance().multicastThread();
        multicastThreadExecutor.execute(new Runnable() {
            @Override
            public void run() {
                startListening();
            }
        });
        return super.onStartCommand(intent, flags, startId);
    }

    private void startListening() {
        Log.i(TAG, "startListening: ");
        while(true) {
            Log.i(TAG, "startListening: iterando...");
            Socket socket = null;
            try {
                ServerSocket serverSocket = new ServerSocket(LISTENING_PORT);
                if (socket == null)
                    socket = serverSocket.accept();
                String ip = socket.getInetAddress().getHostAddress();
                InputStream inputStream = socket.getInputStream();
                DataInputStream dataInputStream = new DataInputStream(inputStream);
                int type = dataInputStream.readInt();
                int length = dataInputStream.readInt();
                byte[] data = new byte[length];
                if (length > 0)
                    dataInputStream.readFully(data);
                serverSocket.close();
                socket.close();
                switch (type){
                    case MessageType.MESSAGE_TYPE_STREAMING_REQUEST:
                        EventBus.getDefault().post(new StreamingRequestEvent(ip, new String(data, "UTF-8")));
                        break;
                    case MessageType.MESSAGE_TYPE_STREAMING_OK:
                        EventBus.getDefault().post(new StreamingOkEvent());
                        break;
                }
            } catch (IOException e) {
                Log.i(TAG, "startListening: " + e.getMessage());
                break;
            }
        }
    }

    private void stopService(){
        if(multicastThreadExecutor != null)
            ((AppExecutors.MulticastThreadExecutor)multicastThreadExecutor).shutdown();
    }

    @Override
    public void onDestroy() {
        stopService();
        super.onDestroy();
    }
}
