package vjgarciag96.chatrcmm.data.event;

/**
 * Created by victor on 2/04/18.
 */

public class StreamingRequestEvent {

    private String requestIp;
    private String streamingUrl;

    public StreamingRequestEvent(String requestIp, String streamingUrl) {
        this.requestIp = requestIp;
        this.streamingUrl = streamingUrl;
    }

    public String getRequestIp() {
        return requestIp;
    }

    public void setRequestIp(String requestIp) {
        this.requestIp = requestIp;
    }

    public String getStreamingUrl() {
        return streamingUrl;
    }

    public void setStreamingUrl(String streamingUrl) {
        this.streamingUrl = streamingUrl;
    }
}
