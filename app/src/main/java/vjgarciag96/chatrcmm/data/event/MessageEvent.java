package vjgarciag96.chatrcmm.data.event;

/**
 * Created by victor on 12/03/18.
 */

public class MessageEvent{

    private int code;
    private byte[] data;
    private String senderAddress;

    public MessageEvent(int code, byte[] data, String senderAddress) {
        this.code = code;
        this.data = data;
        this.senderAddress = senderAddress;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public String getSenderAddress() {
        return senderAddress;
    }

    public void setSenderAddress(String senderAddress) {
        this.senderAddress = senderAddress;
    }
}
