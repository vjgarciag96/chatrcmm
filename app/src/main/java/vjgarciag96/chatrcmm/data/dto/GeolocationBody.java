
package vjgarciag96.chatrcmm.data.dto;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GeolocationBody {

    @SerializedName("considerIp")
    @Expose
    private String considerIp;
    @SerializedName("wifiAccessPoints")
    @Expose
    private List<WifiAccessPoint> wifiAccessPoints = null;
    @SerializedName("cellTowers")
    @Expose
    private List<CellTower> cellTowers = null;

    public String getConsiderIp() {
        return considerIp;
    }

    public void setConsiderIp(String considerIp) {
        this.considerIp = considerIp;
    }

    public List<WifiAccessPoint> getWifiAccessPoints() {
        return wifiAccessPoints;
    }

    public void setWifiAccessPoints(List<WifiAccessPoint> wifiAccessPoints) {
        this.wifiAccessPoints = wifiAccessPoints;
    }

    public List<CellTower> getCellTowers() {
        return cellTowers;
    }

    public void setCellTowers(List<CellTower> cellTowers) {
        this.cellTowers = cellTowers;
    }

}
