
package vjgarciag96.chatrcmm.data.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GeolocationResponse {

    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("accuracy")
    @Expose
    private Double accuracy;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Double accuracy) {
        this.accuracy = accuracy;
    }

}
