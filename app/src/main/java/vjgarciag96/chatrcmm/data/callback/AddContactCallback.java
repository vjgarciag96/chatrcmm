package vjgarciag96.chatrcmm.data.callback;

/**
 * Created by victor on 1/03/18.
 */

public interface AddContactCallback {
    void onContactAdded(String name, String ip, String port);
}
