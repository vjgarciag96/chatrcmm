package vjgarciag96.chatrcmm.data.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;

import vjgarciag96.chatrcmm.data.model.Message;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by victor on 11/03/18.
 */
@Dao
public interface MessageDAO {

    @Insert(onConflict = REPLACE)
    long insertMessage(Message message);

}
