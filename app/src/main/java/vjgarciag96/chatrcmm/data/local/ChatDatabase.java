package vjgarciag96.chatrcmm.data.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import vjgarciag96.chatrcmm.data.local.dao.ContactDAO;
import vjgarciag96.chatrcmm.data.local.dao.MessageDAO;
import vjgarciag96.chatrcmm.data.model.Contact;
import vjgarciag96.chatrcmm.data.model.Message;

/**
 * Created by victor on 11/03/18.
 */
@Database(entities = {Contact.class, Message.class}, version = 5,
        exportSchema = false)
public abstract class ChatDatabase extends RoomDatabase {

    public static final String DATABASE_NAME = "chat";

    // For Singleton instantiation
    private static final Object LOCK = new Object();
    private static volatile ChatDatabase sInstance;

    public static ChatDatabase getInstance(Context context) {
        if (sInstance == null) {
            synchronized (LOCK) {
                if (sInstance == null) {
                    sInstance = Room.databaseBuilder(context.getApplicationContext(),
                            ChatDatabase.class, DATABASE_NAME)
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return sInstance;
    }

    public abstract ContactDAO contactDAO();
    public abstract MessageDAO messageDAO();
}
