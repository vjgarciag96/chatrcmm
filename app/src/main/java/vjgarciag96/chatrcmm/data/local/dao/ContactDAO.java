package vjgarciag96.chatrcmm.data.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import java.util.List;

import vjgarciag96.chatrcmm.data.model.Contact;
import vjgarciag96.chatrcmm.data.model.ContactAndMessages;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by victor on 11/03/18.
 */
@Dao
public interface ContactDAO {

    @Query("SELECT * FROM Contact")
    List<Contact> loadContacts();

    @Transaction
    @Query("SELECT * FROM Contact Where contactId = :contactId")
    ContactAndMessages loadContactsAndMessages(long contactId);

    @Query("SELECT * FROM Contact Where ipAddress = :ipAddress")
    Contact getContactByIp(String ipAddress);

    @Insert(onConflict = REPLACE)
    long[] insertAllContacts(List<Contact> contacts);

    @Insert(onConflict = REPLACE)
    long insertContact(Contact contact);

}
