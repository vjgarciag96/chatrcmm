package vjgarciag96.chatrcmm.data.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import java.util.List;

/**
 * Created by victor on 11/03/18.
 */
@Entity(tableName = "Contact", indices = {@Index(value = {"contactId"})})
public class Contact {

    @PrimaryKey(autoGenerate = true)
    private long contactId;
    private String name;
    private String ipAddress;
    private int port;

    public Contact(long contactId, String name, String ipAddress, int port) {
        this.contactId = contactId;
        this.name = name;
        this.ipAddress = ipAddress;
        this.port = port;
    }

    @Ignore
    public Contact(String name, String ipAddress, int port) {
        this.name = name;
        this.ipAddress = ipAddress;
        this.port = port;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public long getContactId() {
        return contactId;
    }

    public void setContactId(long contactId) {
        this.contactId = contactId;
    }
}
