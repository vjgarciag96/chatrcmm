package vjgarciag96.chatrcmm.data.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.graphics.Bitmap;

import java.io.File;

@Entity(tableName = "Message",
        indices = {@Index(value = {"contactId"})},
        foreignKeys = @ForeignKey(entity = Contact.class,
                parentColumns = "contactId",
                childColumns = "contactId"))
public class Message {

    @PrimaryKey(autoGenerate = true) private long messageId;
    private String fromName;
    private boolean isSelf;
    private boolean received;
    private long contactId;
    private int contentType;
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB) private byte[] content;
    @Ignore private File image;

    public Message(long messageId, String fromName, boolean isSelf, boolean received, long contactId, int contentType, byte[] content) {
        this.messageId = messageId;
        this.fromName = fromName;
        this.isSelf = isSelf;
        this.received = received;
        this.contactId = contactId;
        this.contentType = contentType;
        this.content = content;
    }

    @Ignore
    public Message(String fromName, boolean isSelf, boolean received, long contactId, int contentType, byte[] content) {
        this.fromName = fromName;
        this.isSelf = isSelf;
        this.received = received;
        this.contactId = contactId;
        this.contentType = contentType;
        this.content = content;
    }

    @Ignore
    public Message(String fromName, boolean isSelf, boolean received, int contentType, byte[] content) {
        this.fromName = fromName;
        this.isSelf = isSelf;
        this.received = received;
        this.contentType = contentType;
        this.content = content;
    }

    @Ignore
    public Message(String fromName, boolean isSelf, int contentType, byte[] content) {
        this.fromName = fromName;
        this.isSelf = isSelf;
        this.contentType = contentType;
        this.content = content;
    }

    @Ignore
    public Message(String fromName, boolean isSelf, boolean received, int contentType, File image) {
        this.fromName = fromName;
        this.isSelf = isSelf;
        this.received = received;
        this.contentType = contentType;
        this.image = image;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public boolean isSelf() {
        return isSelf;
    }

    public void setSelf(boolean isSelf) {
        this.isSelf = isSelf;
    }

    public long getMessageId() {
        return messageId;
    }

    public void setMessageId(long messageId) {
        this.messageId = messageId;
    }

    public boolean isReceived() {
        return received;
    }

    public void setReceived(boolean received) {
        this.received = received;
    }

    public long getContactId() {
        return contactId;
    }

    public void setContactId(long contactId) {
        this.contactId = contactId;
    }

    public File getImage() {
        return image;
    }

    public void setImage(File image) {
        this.image = image;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public int getContentType() {
        return contentType;
    }

    public void setContentType(int contentType) {
        this.contentType = contentType;
    }
}
