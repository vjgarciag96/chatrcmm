package vjgarciag96.chatrcmm.data.model;

import java.net.SocketException;

import vjgarciag96.chatrcmm.common.SocketAddressValidator;

/**
 * Created by victor on 24/03/18.
 */

public class MessageCreator {

    public static Message create(int messageType, byte[] data, String sender, long contactId) throws SocketException {
        return new Message(sender, SocketAddressValidator.isLocalhost(sender),
                true, contactId, messageType, data);
//        switch (messageType) {
//            case MessageType.MESSAGE_TYPE_TEXT:
//                String message = new String(data, StandardCharsets.UTF_8);
//                message = message.replaceAll("\0+$", "");
//            case MessageType.MESSAGE_TYPE_IMAGE:
//                File file = new File(context.getCacheDir(), "received-image");
//                file.createNewFile();
//                FileOutputStream fileOutputStream = new FileOutputStream(file);
//                fileOutputStream.write(data);
//                fileOutputStream.flush();
//                fileOutputStream.close();
//                //TODO persist Image
//                return new Message(sender, "Imagen",
//                        IpAddressValidator.isLocalhost(sender), true, contactId);
//        }
//        return null;
    }
}
