package vjgarciag96.chatrcmm.data.model;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Relation;

import java.util.List;

/**
 * Created by victor on 11/03/18.
 */

public class ContactAndMessages {

    @Embedded
    public Contact contact;

    @Relation(parentColumn = "contactId", entityColumn = "contactId")
    public List<Message> messages;

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
