package vjgarciag96.chatrcmm.data.mapper;

import java.util.Arrays;
import java.util.List;

public class ChannelFrequencyMapper extends BaseMapper<Integer, Integer> {

    private final static List<Integer> channelsFrequency =
            Arrays.asList(0, 2412, 2417, 2422, 2427, 2432, 2437, 2442, 2447,
                    2452, 2457, 2462, 2467, 2472, 2484);

    @Override
    Integer map(Integer toMap) {
        return channelsFrequency.indexOf(toMap);
    }
}
