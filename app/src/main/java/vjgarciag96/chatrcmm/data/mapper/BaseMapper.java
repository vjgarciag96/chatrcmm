package vjgarciag96.chatrcmm.data.mapper;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseMapper<I, O> {
    abstract O map(I toMap);

    public List<O> mapList(List<I> toMap) {
        List<O> mappedItems = new ArrayList<>();
        for (I item : toMap)
            mappedItems.add(map(item));
        return mappedItems;
    }
}
