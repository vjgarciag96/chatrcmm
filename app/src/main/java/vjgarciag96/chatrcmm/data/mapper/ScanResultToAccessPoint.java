package vjgarciag96.chatrcmm.data.mapper;

import android.net.wifi.ScanResult;

import vjgarciag96.chatrcmm.data.dto.WifiAccessPoint;

public class ScanResultToAccessPoint extends BaseMapper<ScanResult, WifiAccessPoint> {
    @Override
    WifiAccessPoint map(ScanResult toMap) {
        return new WifiAccessPoint(toMap.BSSID, toMap.level, new ChannelFrequencyMapper().map(Integer.valueOf(toMap.frequency)));
    }
}
