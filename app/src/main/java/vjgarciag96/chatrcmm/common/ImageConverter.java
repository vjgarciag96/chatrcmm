package vjgarciag96.chatrcmm.common;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * Created by victor on 25/03/18.
 */

public class ImageConverter {

    private static final String TAG = "image-converter";

    public static File getImageFileFromBytes(Context context, byte[] rawImage){
        File imageFile = new File(context.getCacheDir(), "received-image" + System.currentTimeMillis());
        try {
            if(imageFile.exists())
                imageFile.delete();
            imageFile.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(imageFile);
            fileOutputStream.write(rawImage);
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
        return imageFile;
    }

    public static File getFileFromImagePath(Context context, String imagePath){
        return getImageFileFromBytes(context, getBytesFromBitmap(getBitmapFromImagePath(imagePath), 70));
    }

    private static Bitmap getBitmapFromImagePath(String imagePath) {
        File imgFile = new File(imagePath);
        Bitmap myBitmap = null;
        if (imgFile.exists())
            myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        return myBitmap;
    }

    private static byte[] getBytesFromBitmap(Bitmap bitmap, int quality) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG
                , quality, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

}
