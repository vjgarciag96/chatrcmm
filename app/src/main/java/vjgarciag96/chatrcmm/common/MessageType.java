package vjgarciag96.chatrcmm.common;

/**
 * Created by victor on 24/03/18.
 */

public interface MessageType {
    int MESSAGE_TYPE_TEXT = 0;
    int MESSAGE_TYPE_IMAGE = 1;
    int MESSAGE_TYPE_STREAMING_REQUEST = 2;
    int MESSAGE_TYPE_STREAMING_OK = 3;
    int MESSAGE_TYPE_LOCATION = 4;
}