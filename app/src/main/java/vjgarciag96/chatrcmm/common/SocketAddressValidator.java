package vjgarciag96.chatrcmm.common;

import android.text.TextUtils;
import android.util.Log;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

/**
 * Created by victor on 12/03/18.
 */

public class SocketAddressValidator {

    private static final String TAG = "IpAddressValidator";

    private static final String LOOPBACK_ADDRESS = "127.0.0.1";

    public static String getWrongIpAddressMessages(String ipAddress) {
        if (ipAddress == null || TextUtils.isEmpty(ipAddress))
            return "El campo IP no puede estar vacio";
        if (!ipAddress.contains("."))
            return "Los bloques de la IP tienen que estar separados por puntos: XXX.XXX.XXX.XXX";
        String[] splittedAddress = ipAddress.split("\\.");
        if (splittedAddress.length != 4)
            return "Tiene que haber cuatro bloques separados por puntos: XXX.XXX.XXX.XXX";
        for (int i = 0; i < splittedAddress.length; i++) {
            try {
                int ipSlot = Integer.valueOf(splittedAddress[i]);
                if (ipSlot < 0 || ipSlot > 255)
                    return "Los valores de los bloques tienen que estar entre 0 y 255";
                else if (i == 3) {
                    if (ipSlot == 0 || ipSlot == 255)
                        return "El último bloque no puede contener los valores 0 o 255";
                }
            } catch (Exception notInteger) {
                return "Los bloques tienen que estar compuestos por número enteros";
            }
        }
        return null;
    }

    public static String getWrongPortMessage(String portMessage) {
        if(portMessage == null || TextUtils.isEmpty(portMessage))
            return "El valor de puerto no puede estar vacio";
        int port = Integer.valueOf(portMessage);
        if (port < 0 || port > 65535)
            return "Los valores de puerto tienen rango entre 0-65535";
        if (port <= 1024)
            return "No puedes utilizar un puerto bien conocido (0-1024)";
        if (port >= 49151)
            return "No puedes utilizar un puerto privado/dinámico (49151-65535)";
        if (port == 4445)
            return "No puedes utilizar el puerto 4445 (Reservado para chat individuales)";
        return null;
    }

    public static boolean isMulticast(String ipAddress) {
        String[] splittedIp = ipAddress.split("\\.");
        int firstBlock = Integer.valueOf(splittedIp[0]);
        return (firstBlock >= 224 && firstBlock <= 239);
    }

    private static List<Inet4Address> getInet4Addresses() throws SocketException {
        List<Inet4Address> ret = new ArrayList<>();

        Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();
        for (NetworkInterface netint : Collections.list(nets)) {
            Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
            for (InetAddress inetAddress : Collections.list(inetAddresses)) {
                if (inetAddress instanceof Inet4Address && !inetAddress.isLoopbackAddress()) {
                    ret.add((Inet4Address) inetAddress);
                }
            }
        }

        return ret;
    }

    /**
     * Returns this host's first non-loopback IPv4 address string in textual
     * representation.
     *
     * @return
     * @throws SocketException
     */
    public static String getHost4Address() throws SocketException {
        List<Inet4Address> inet4 = getInet4Addresses();
        return !inet4.isEmpty()
                ? inet4.get(0).getHostAddress()
                : null;
    }

    public static boolean isLocalhost(String address) throws SocketException {
        return getHost4Address().equals(address) || getHost4Address().equals(LOOPBACK_ADDRESS);
    }
}
