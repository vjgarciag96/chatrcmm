package vjgarciag96.chatrcmm.common;

/**
 * Created by victor on 11/03/18.
 */

public interface BundleKeys {
    public static final String CONTACT_ID_KEY = "contact-id";
    public static final String MULTICAST_ADDRESS_KEY = "multicast-address";
    public static final String MULTICAST_PORT_KEY = "multicast-port";
    public static final String IMAGE_PATH_KEY = "image-path";
    public static final String TARGET_IP_ADDRESS_KEY = "target-ip-address";
    public static final String TARGET_PORT_KEY = "target-port-address";
    public static final String CHAT_TYPE_KEY = "chat-type";
    public static final String SENDER_IP_ADDRESS_KEY = "sender-ip-address";
    public static final String STREAMING_URL_KEY = "streaming-url";
}
