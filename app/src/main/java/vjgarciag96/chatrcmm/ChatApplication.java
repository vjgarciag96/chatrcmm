package vjgarciag96.chatrcmm;

import android.app.Application;
import android.content.Intent;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

import com.facebook.drawee.backends.pipeline.Fresco;

import vjgarciag96.chatrcmm.data.service.MessagesPullService;

/**
 * Created by victor on 24/03/18.
 */

public class ChatApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        PreferenceManager.setDefaultValues(this, R.xml.chat_preferences, false);
        Fresco.initialize(this);
        startListening();
    }

    private void startListening() {
        Intent individualChatListenerService = new Intent(this, MessagesPullService.class);
        startService(individualChatListenerService);
    }
}
