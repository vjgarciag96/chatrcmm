package vjgarciag96.chatrcmm.ui.contactlist;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import vjgarciag96.chatrcmm.common.SocketAddressValidator;
import vjgarciag96.chatrcmm.data.callback.AddContactCallback;
import vjgarciag96.chatrcmm.R;

/**
 * Created by victor on 1/03/18.
 */

public class AddContactFragment extends Fragment {

    private static final String TAG = "ConfigurationFragment";

    @BindView(R.id.name_text_box)
    EditText nameEditText;
    @BindView(R.id.ip_dir_text_box)
    EditText ipDirEditText;
    @BindView(R.id.port_text_box)
    EditText portEditText;

    public static AddContactFragment newInstance() {
        AddContactFragment fragment = new AddContactFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.add_contact_fragment, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @OnClick(R.id.ok_button)
    public void onOkButtonClicked() {
        String name = "";
        String ipDir = "";
        String port = "";
        if (nameEditText.getText() != null)
            name = nameEditText.getText().toString();
        if (ipDirEditText.getText() != null)
            ipDir = ipDirEditText.getText().toString();
        if (portEditText.getText() != null)
            port = portEditText.getText().toString();
        if(TextUtils.isEmpty(name))
            name = "Sin nombre";
        String wrongIpMessage = SocketAddressValidator.getWrongIpAddressMessages(ipDir);
        if (wrongIpMessage != null)
            Toast.makeText(getActivity(), wrongIpMessage, Toast.LENGTH_SHORT).show();
        else {
            if(SocketAddressValidator.isMulticast(ipDir)) {
                String portMessage = SocketAddressValidator.getWrongPortMessage(port);
                if (portMessage != null)
                    Toast.makeText(getActivity(), portMessage, Toast.LENGTH_SHORT).show();
                else
                    ((AddContactCallback) getActivity()).onContactAdded(name, ipDir, port);
            }else
                ((AddContactCallback) getActivity()).onContactAdded(name, ipDir, port);
        }
    }

}
