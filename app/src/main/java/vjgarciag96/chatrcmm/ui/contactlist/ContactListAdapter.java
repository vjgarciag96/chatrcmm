package vjgarciag96.chatrcmm.ui.contactlist;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import vjgarciag96.chatrcmm.common.BundleKeys;
import vjgarciag96.chatrcmm.R;
import vjgarciag96.chatrcmm.common.SocketAddressValidator;
import vjgarciag96.chatrcmm.data.model.Contact;
import vjgarciag96.chatrcmm.ui.chat.ChatActivity;

/**
 * Created by victor on 11/03/18.
 */

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ContactViewHolder> {

    private static final String TAG = "ContactListAdapter";

    private List<Contact> contactList;
    private ViewGroup parent;

    public ContactListAdapter() {
        this.contactList = new ArrayList<>();
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.parent = parent;

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_list_item, parent, false);
        ContactViewHolder contactViewHolder = new ContactViewHolder(view);

        return contactViewHolder;
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {
        final Contact contact = contactList.get(position);
        if (SocketAddressValidator.isMulticast(contact.getIpAddress())) {
            Picasso.with(parent.getContext())
                    .load("http://stagtv.co.uk/wp-content/uploads/2017/07/group-of-users-silhouette_318-49953.jpg")
                    .into(holder.contactProfileImageView);
            holder.contactItemHolderView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent chatIntent = new Intent(v.getContext(), ChatActivity.class);
                    chatIntent.putExtra(BundleKeys.CHAT_TYPE_KEY, "group");
                    chatIntent.putExtra(BundleKeys.CONTACT_ID_KEY, contact.getContactId());
                    v.getContext().startActivity(chatIntent);
                }
            });
        }else {
            Picasso.with(parent.getContext())
                    .load("https://upload.wikimedia.org/wikipedia/commons/7/70/User_icon_BLACK-01.png")
                    .into(holder.contactProfileImageView);
            holder.contactItemHolderView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent chatIntent = new Intent(v.getContext(), ChatActivity.class);
                    chatIntent.putExtra(BundleKeys.CONTACT_ID_KEY, contact.getContactId());
                    v.getContext().startActivity(chatIntent);
                }
            });
        }
        holder.contactNameTextView.setText(contact.getName());
        holder.contactIpTextView.setText(contact.getIpAddress());
        holder.contactPortTextView.setText(String.valueOf(contact.getPort()));

    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public void addContacts(List<Contact> contacts) {
        contactList.clear();
        contactList.addAll(contacts);
        notifyDataSetChanged();
    }

    public void addContact(Contact contact) {
        contactList.add(contact);
        notifyDataSetChanged();
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.contact_item_holder_view)
        ConstraintLayout contactItemHolderView;
        @BindView(R.id.contact_profile_image_view)
        ImageView contactProfileImageView;
        @BindView(R.id.contact_ip_text_view)
        TextView contactIpTextView;
        @BindView(R.id.contact_name_text_view)
        TextView contactNameTextView;
        @BindView(R.id.contact_port_text_view)
        TextView contactPortTextView;

        public ContactViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
