package vjgarciag96.chatrcmm.ui.contactlist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.net.SocketException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import vjgarciag96.chatrcmm.common.SocketAddressValidator;
import vjgarciag96.chatrcmm.data.callback.AddContactCallback;
import vjgarciag96.chatrcmm.common.AppExecutors;
import vjgarciag96.chatrcmm.R;
import vjgarciag96.chatrcmm.data.local.ChatDatabase;
import vjgarciag96.chatrcmm.data.model.Contact;

/**
 * Created by victor on 11/03/18.
 */

public class ContactListActivity extends AppCompatActivity
        implements AddContactCallback {

    private static final String TAG = "ContactListActivity";

    @BindView(R.id.my_ip_content_text_view) TextView myIpContent;
    @BindView(R.id.contact_list_wrapper) ConstraintLayout contactListWrapper;
    @BindView(R.id.contact_list_recycler_view) RecyclerView contactListRecyclerView;
    private RecyclerView.LayoutManager contactListLayoutManager;
    private RecyclerView.Adapter contactListAdapter;

    private Fragment addContactFragment;
    private Fragment chatConfigFragment;
    private List<Contact> contacts;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_list_activity);
        ButterKnife.bind(this);
        initViews();
    }

    private void initViews(){
        try {
            myIpContent.setText(SocketAddressValidator.getHost4Address());
        } catch (SocketException e) {
            myIpContent.setText(R.string.unknown_ip);
            Toast.makeText(this, R.string.no_ip_warning, Toast.LENGTH_SHORT);
        }
        createContactListRecyclerView();
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle("Lista de contactos");
    }

    private void createContactListRecyclerView() {
        contactListRecyclerView.setHasFixedSize(true);

        contactListLayoutManager = new LinearLayoutManager(this);
        ((LinearLayoutManager) contactListLayoutManager).setOrientation(LinearLayoutManager.VERTICAL);
        contactListRecyclerView.setLayoutManager(contactListLayoutManager);
        contactListRecyclerView.addItemDecoration(new DividerItemDecoration(contactListRecyclerView.getContext(),
                ((LinearLayoutManager) contactListLayoutManager).getOrientation()));

        contactListAdapter = new ContactListAdapter();
        contactListRecyclerView.setAdapter(contactListAdapter);
        populateContactList();
    }

    private void populateContactList() {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                contacts = ChatDatabase.getInstance(getBaseContext()).contactDAO().loadContacts();
                onContactListPopulated();
            }
        });
    }

    private void onContactListPopulated() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((ContactListAdapter) contactListAdapter).addContacts(contacts);
            }
        });
    }

    @OnClick(R.id.add_contact_fab)
    public void onAddContactButtonPressed() {
        showAddContactFragment();
    }

    private void showAddContactFragment() {
        addContactFragment = AddContactFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.contact_list_frame_layout, addContactFragment, "add-contact-fragment");
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void hideAddContactFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (addContactFragment.isAdded())
            transaction.remove(addContactFragment);
        transaction.commit();
    }

    private void showChatConfigFragment() {
        chatConfigFragment = ChatPreferencesFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.contact_list_frame_layout, chatConfigFragment, "chat-config-fragment");
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void hideChatConfigFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (chatConfigFragment.isAdded())
            transaction.remove(chatConfigFragment);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (addContactFragment != null
                && addContactFragment.isVisible())
            hideAddContactFragment();
        if (chatConfigFragment != null
                && chatConfigFragment.isVisible()) {
            hideChatConfigFragment();
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setTitle("Lista de contactos");
        }
        super.onBackPressed();
    }

    @Override
    public void onContactAdded(String name, String ip, String port) {
        final Contact contact = new Contact(name, ip, Integer.valueOf(port));
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                long id = ChatDatabase.getInstance(getBaseContext())
                        .contactDAO().insertContact(contact);
                if (id > -1)
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getBaseContext(), "Contacto " + contact.getIpAddress() + " añadido con éxito", Toast.LENGTH_SHORT).show();
                            populateContactList();
                        }
                    });
            }
        });

        ((ContactListAdapter) contactListAdapter).addContact(new Contact(name, ip, Integer.valueOf(port)));
        hideAddContactFragment();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_config, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_config:
                showChatConfigFragment();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
