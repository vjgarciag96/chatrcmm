package vjgarciag96.chatrcmm.ui.chat;


import java.io.UnsupportedEncodingException;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.squareup.picasso.Picasso;

import vjgarciag96.chatrcmm.R;
import vjgarciag96.chatrcmm.common.ImageConverter;
import vjgarciag96.chatrcmm.common.MessageType;
import vjgarciag96.chatrcmm.data.model.Message;

public class MessagesListAdapter extends BaseAdapter {

    private static final String TAG = "MessagesListAdapter";

    private Context context;
    private List<Message> messages;

    public MessagesListAdapter(Context context, List<Message> messages) {
        this.context = context;
        this.messages = messages;
    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int position) {
        return messages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Message message = messages.get(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        switch (message.getContentType()) {
            case MessageType.MESSAGE_TYPE_TEXT:
                if (message.isSelf())
                    convertView = mInflater.inflate(R.layout.list_item_message_right, null);
                else
                    convertView = mInflater.inflate(R.layout.list_item_message_left, null);
                TextView lblFrom = convertView.findViewById(R.id.lblMsgFrom);
                TextView txtMsg = convertView.findViewById(R.id.txtMsg);
                try {
                    String messageText = new String(message.getContent(), "UTF-8");
                    txtMsg.setText(messageText);
                    lblFrom.setText(message.getFromName());
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                break;
            case MessageType.MESSAGE_TYPE_IMAGE:
                if (message.isSelf())
                    convertView = mInflater.inflate(R.layout.list_item_image_right, null);
                else
                    convertView = mInflater.inflate(R.layout.list_item_image_left, null);
                SimpleDraweeView imageMsg = convertView.findViewById(R.id.image_msg);
                imageMsg.getHierarchy().setActualImageScaleType(ScalingUtils.ScaleType.CENTER_CROP);
                if (message.getImage() == null)
                    message.setImage(ImageConverter.getImageFileFromBytes(context, message.getContent()));
                imageMsg.setImageURI(Uri.parse("file://" + message.getImage().getAbsolutePath()));
                break;
            case MessageType.MESSAGE_TYPE_LOCATION:
                String imageURL = "";
                if (message.isSelf())
                    convertView = mInflater.inflate(R.layout.list_item_location_right, null);
                else
                    convertView = mInflater.inflate(R.layout.list_item_location_left, null);
                SimpleDraweeView locationImage = convertView.findViewById(R.id.map_image);
                try {
                    String[] splittedLocation = new String(message.getContent(), "UTF-8").split(",");
                    final String lat = splittedLocation[0];
                    final String lng = splittedLocation[1];
                    Log.i(TAG, "Location added to inteface \n" +
                            "lat = " + lat + "\n" +
                            "lng = " + lng + "\n");
                    imageURL = String.format("http://maps.googleapis.com/maps/api/staticmap?center=%s,%s&size=%dx%d&sensor=false&maptype=roadmap&zoom=17&markers=color:red|%s,%s&key=AIzaSyD83J8Wo2Yl49fhF4fWc3mNbheq1AHiFLQ",
                            lat, lng, 300, 300, lat, lng);
                    Picasso.with(context).load(imageURL).into(locationImage);
                    locationImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Uri gmmIntentUri = Uri.parse(String.format("geo:%s,%s", lat, lng));
                            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                            mapIntent.setPackage("com.google.android.apps.maps");
                            if (mapIntent.resolveActivity(context.getPackageManager()) != null) {
                                context.startActivity(mapIntent);
                            }
                        }
                    });
                    Log.i(TAG, "getView: " + imageURL);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }

        return convertView;
    }

    public void addMessage(Message message) {
        messages.add(message);
        notifyDataSetChanged();
    }

    public void loadMessages(List<Message> messages) {
        this.messages = messages;
        notifyDataSetChanged();
    }
}
