package vjgarciag96.chatrcmm.ui.chat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.view.ContextThemeWrapper;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.common.util.UriUtil;
import com.facebook.drawee.view.SimpleDraweeView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vjgarciag96.chatrcmm.common.AppExecutors;
import vjgarciag96.chatrcmm.common.BundleKeys;
import vjgarciag96.chatrcmm.R;
import vjgarciag96.chatrcmm.common.ImageConverter;
import vjgarciag96.chatrcmm.common.SocketAddressValidator;
import vjgarciag96.chatrcmm.common.MessageType;
import vjgarciag96.chatrcmm.common.StreamingConfiguration;
import vjgarciag96.chatrcmm.data.dto.GeolocationBody;
import vjgarciag96.chatrcmm.data.dto.GeolocationResponse;
import vjgarciag96.chatrcmm.data.dto.Location;
import vjgarciag96.chatrcmm.data.event.StreamingRequestEvent;
import vjgarciag96.chatrcmm.data.local.ChatDatabase;
import vjgarciag96.chatrcmm.data.event.MessageEvent;
import vjgarciag96.chatrcmm.data.mapper.ScanResultToAccessPoint;
import vjgarciag96.chatrcmm.data.model.ContactAndMessages;
import vjgarciag96.chatrcmm.data.model.Message;
import vjgarciag96.chatrcmm.data.remote.RetrofitClient;
import vjgarciag96.chatrcmm.data.service.MulticastSocketListenerService;
import vjgarciag96.chatrcmm.data.service.SendImageService;
import vjgarciag96.chatrcmm.data.service.StreamingListeningService;
import vjgarciag96.chatrcmm.ui.contactlist.ChatPreferencesFragment;
import vjgarciag96.chatrcmm.ui.streaming.PlayStreamingActivity;
import vjgarciag96.chatrcmm.ui.streaming.PublishStreamingActivity;

public class ChatActivity extends AppCompatActivity
        implements ActivityCompat.OnRequestPermissionsResultCallback {

    private static final String TAG = "MainActivity";
    private static final int DEFAULT_CONTACT_ID = -1;
    private static final int PICK_ACTIVITY_CODE = 1;
    private static final int REQUEST_READ_EXTERNAL_STORAGE = 1;
    private static final int MAX_BUFFER = 65507;

    @BindView(R.id.inputMsg)
    EditText messageInputEditText;

    private String ip = "";
    private int port = -1;

    private MessagesListAdapter adapter;
    private List<Message> messages;

    @BindView(R.id.list_view_messages)
    ListView messagesListView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.profile_image_drawee_view)
    SimpleDraweeView profileImageView;
    @BindView(R.id.chat_name_text_view)
    TextView chatNameTextView;

    private long contactId;

    private MulticastSocket groupSocket;
    private Intent multicastSocketListenerServiceIntent;
    private Intent streamingListenerServiceIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_activity);
        ButterKnife.bind(this);
        initViews();

        StrictMode.ThreadPolicy policy = new
                StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.
                setThreadPolicy(policy);

        Intent chatIntent = getIntent();
        contactId = chatIntent.getLongExtra(BundleKeys.CONTACT_ID_KEY, DEFAULT_CONTACT_ID);
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                final ContactAndMessages contactAndMessages = ChatDatabase.getInstance(getBaseContext()).contactDAO().loadContactsAndMessages(contactId);
                ip = contactAndMessages.getContact().getIpAddress();
                port = contactAndMessages.getContact().getPort();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String name = contactAndMessages.getContact().getName();
                        setChatTitleViews(name + " (" + ip + ")", SocketAddressValidator.isMulticast(ip));
                    }
                });

                if (!SocketAddressValidator.isMulticast(ip)) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.loadMessages(contactAndMessages.getMessages());
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    });
                } else {
                    joinGroup();
                    progressBar.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        stopListeningGroup();
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        streamingListenerServiceIntent = new Intent(this, StreamingListeningService.class);
        streamingListenerServiceIntent.putExtra(BundleKeys.SENDER_IP_ADDRESS_KEY, ip);
        startService(streamingListenerServiceIntent);
    }

    @Override
    protected void onStop() {
        if (streamingListenerServiceIntent != null)
            stopService(streamingListenerServiceIntent);
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void initViews() {
        messages = new ArrayList<>();
        adapter = new MessagesListAdapter(this, messages);
        messagesListView.setAdapter(adapter);
        progressBar.setIndeterminate(true);
    }

    private void setChatTitleViews(String chatName, boolean group) {
        chatNameTextView.setText(chatName);
        if (group)
            profileImageView.setImageURI(new Uri.Builder()
                    .scheme(UriUtil.LOCAL_RESOURCE_SCHEME)
                    .path(String.valueOf(R.drawable.ic_group_chat))
                    .build());
        else
            profileImageView.setImageURI(new Uri.Builder()
                    .scheme(UriUtil.LOCAL_RESOURCE_SCHEME)
                    .path(String.valueOf(R.drawable.ic_user_chat))
                    .build());
    }

    private void joinGroup() {
        startListeningGroup(ip, port);
    }

    private void startListeningGroup(String groupAddress, int port) {
        multicastSocketListenerServiceIntent = new Intent(this, MulticastSocketListenerService.class);
        multicastSocketListenerServiceIntent.putExtra(BundleKeys.MULTICAST_ADDRESS_KEY, groupAddress);
        multicastSocketListenerServiceIntent.putExtra(BundleKeys.MULTICAST_PORT_KEY, port);
        startService(multicastSocketListenerServiceIntent);
    }

    private void stopListeningGroup() {
        if (multicastSocketListenerServiceIntent != null)
            stopService(multicastSocketListenerServiceIntent);
    }

    private void sendLocation(final Message message) throws UnknownHostException {
        InetAddress ipAddress = InetAddress.getByName(ip);
        byte[] buffer = message.getContent();
        try {
            Socket socket = new Socket(ipAddress, 4445);
            OutputStream outputStream = socket.getOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
            dataOutputStream.writeInt(message.getContentType());
            dataOutputStream.writeInt(buffer.length);
            dataOutputStream.write(buffer, 0, buffer.length);
            socket.close();
        } catch (IOException e) {
            Log.e(TAG, "sendMessage: " + e.getMessage());
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                messageInputEditText.setText("");
                adapter.addMessage(message);
            }
        });
    }

    private void sendIndividualMessage(final Message message) throws SocketException, UnknownHostException, IOException {
        InetAddress ipAddress = InetAddress.getByName(ip);
        byte[] buffer = message.getContent();
        try {
            Socket socket = new Socket(ipAddress, 4445);
            OutputStream outputStream = socket.getOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
            dataOutputStream.writeInt(message.getContentType());
            dataOutputStream.writeInt(buffer.length);
            dataOutputStream.write(buffer, 0, buffer.length);
            socket.close();
        } catch (IOException e) {
            Log.e(TAG, "sendMessage: " + e.getMessage());
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                messageInputEditText.setText("");
                adapter.addMessage(message);
            }
        });
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                ChatDatabase.getInstance(getBaseContext()).messageDAO().insertMessage(message);
            }
        });
    }

    private void sendGroupMessage(String message) {
        try {
            InetAddress ipAddress = InetAddress.getByName(ip);
            groupSocket = new MulticastSocket(port);
            groupSocket.joinGroup(ipAddress);
            groupSocket.setLoopbackMode(false);
            groupSocket.setSoTimeout(5000);
            message = String.valueOf(MessageType.MESSAGE_TYPE_TEXT) + message;
            byte[] buffer = message.getBytes();
            if (buffer.length > MAX_BUFFER)
                Toast.makeText(this, "El tamaño del mensaje tiene que ser más pequeño", Toast.LENGTH_SHORT).show();
            else {
                DatagramPacket packet = new DatagramPacket(buffer, 0, buffer.length, ipAddress, port);
                groupSocket.send(packet);
                messageInputEditText.setText("");
            }
            groupSocket.leaveGroup(ipAddress);
        } catch (UnknownHostException e) {
            Log.e(TAG, "sendGroupMessage: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "sendGroupMessage: " + e.getMessage());
        }

    }


    @OnClick(R.id.btnSend)
    public void onSendPressed() {
        String message = "";
        if (messageInputEditText.getText() != null)
            message = messageInputEditText.getText().toString();
        try {
            if (!TextUtils.isEmpty(ip) && port > 0) {
                if (!SocketAddressValidator.isMulticast(ip))
                    sendIndividualMessage(new Message("Yo", true, false, contactId, MessageType.MESSAGE_TYPE_TEXT, message.getBytes("UTF-8")));
                else
                    sendGroupMessage(message);
            } else
                Toast.makeText(this, "Necesitas indicar una configuración ip-puerto para enviar el mensaje", Toast.LENGTH_SHORT).show();
            //TODO Change Toast for Snackbar
        } catch (IOException e) {
            Log.i(TAG, "onSendPressed: " + e.getMessage());
        }
    }

    @OnClick(R.id.send_image_button)
    public void onSendImageButtonPressed() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            String[] readExternalStorage = {Manifest.permission.READ_EXTERNAL_STORAGE};
            ActivityCompat.requestPermissions(this, readExternalStorage, REQUEST_READ_EXTERNAL_STORAGE);
        } else
            startPickImageIntent();
    }

    @OnClick(R.id.live_streaming_button)
    public void onLiveStreamingVideoButtonPressed() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String streamAddress = "rtmp://" + sharedPreferences.getString(ChatPreferencesFragment.KEY_PREFERENCES_STREAMING_SERVER_URL, StreamingConfiguration.BASE_URL) + "/live/" + System.currentTimeMillis();
        Log.d(TAG, "streamAddress = " + streamAddress);
        Intent liveStreamingIntent = new Intent(this, PublishStreamingActivity.class);
        liveStreamingIntent.putExtra(BundleKeys.STREAMING_URL_KEY, streamAddress);
        liveStreamingIntent.putExtra(BundleKeys.TARGET_IP_ADDRESS_KEY, ip);
        startActivity(liveStreamingIntent);
    }

    @OnClick(R.id.send_location_button)
    public void onSendLocationButtonPressed() {
        Log.d(TAG, "sending location...");
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        GeolocationBody body = new GeolocationBody();
        body.setWifiAccessPoints(new ScanResultToAccessPoint().mapList(wifiManager.getScanResults()));
        body.setConsiderIp("false");
        RetrofitClient.getGeolocationAPIService()
                .getLocation(getString(R.string.geolocation_api_key), body)
                .enqueue(new Callback<GeolocationResponse>() {
                    @Override
                    public void onResponse(Call<GeolocationResponse> call, Response<GeolocationResponse> response) {
                        if (response.isSuccessful()) {
                            Log.i(TAG, "onResponse: " + response.body().getLocation());
                            Location location = response.body().getLocation();
                            try {
                                sendLocation(new Message(ip, true, false,
                                        MessageType.MESSAGE_TYPE_LOCATION, location.toString().getBytes()));
                            } catch (UnknownHostException e) {
                                e.printStackTrace();
                            }
                        } else
                            Log.i(TAG, "onResponse: not successful");
                    }

                    @Override
                    public void onFailure(Call<GeolocationResponse> call, Throwable t) {
                        Log.i(TAG, "onFailure: " + t.getMessage());
                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_EXTERNAL_STORAGE && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            startPickImageIntent();
        else
            Toast.makeText(this, "Necesitas conceder permisos para enviar imágenes", Toast.LENGTH_SHORT).show();
    }

    private void startPickImageIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, PICK_ACTIVITY_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_ACTIVITY_CODE && data != null) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor =
                    getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            final String picturePath = cursor.getString(columnIndex);
            cursor.close();
            AppExecutors.getInstance().mainThread().execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        adapter.addMessage(new Message(ip, SocketAddressValidator.isLocalhost(ip), true,
                                MessageType.MESSAGE_TYPE_IMAGE, ImageConverter.getFileFromImagePath(getBaseContext(), picturePath)));
                    } catch (SocketException e) {
                        e.printStackTrace();
                    }
                }
            });
            Log.i(TAG, "onActivityResult: " + picturePath);
            AppExecutors.getInstance().networkIO().execute(new Runnable() {
                @Override
                public void run() {
                    sendImage(picturePath);
                }
            });
        }
    }


    private void sendImage(String imagePath) {
        if (!TextUtils.isEmpty(ip) && port > 0) {
            Intent sendMessageIntent = new Intent(this, SendImageService.class);
            sendMessageIntent.putExtra(BundleKeys.IMAGE_PATH_KEY, imagePath);
            sendMessageIntent.putExtra(BundleKeys.TARGET_IP_ADDRESS_KEY, ip);
            sendMessageIntent.putExtra(BundleKeys.TARGET_PORT_KEY, port);
            startService(sendMessageIntent);
        } else
            Toast.makeText(this, "Necesitas indicar una configuración ip-puerto para enviar el mensaje", Toast.LENGTH_SHORT).show();
    }

    @Subscribe
    public void onMessageReceived(MessageEvent messageEvent) {
        Log.i(TAG, "onMessageReceived: " + messageEvent.getCode());
        switch (messageEvent.getCode()) {
            case MessageType.MESSAGE_TYPE_TEXT:
                addTextMessage(messageEvent.getSenderAddress(), messageEvent.getData());
                break;
            case MessageType.MESSAGE_TYPE_IMAGE:
                addImageMessage(messageEvent.getSenderAddress(), messageEvent.getData());
                break;
            case MessageType.MESSAGE_TYPE_LOCATION:
                addLocationMessage(messageEvent.getSenderAddress(), messageEvent.getData());
            default:
                break;
        }
    }

    @Subscribe
    public void onStreamingRequestReceived(final StreamingRequestEvent event) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(ChatActivity.this, R.style.FullScreenDialog));
                builder.setTitle("Llamada de streaming entrante")
                        .setMessage("¿Quieres aceptar la llamada?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    sendStreamingOKMessage(new Message("Yo", true, false, MessageType.MESSAGE_TYPE_STREAMING_OK, "OK".getBytes("UTF-8")));
                                } catch (IOException e) {
                                    Log.e(TAG, e.getMessage());
                                }
                                Intent playStreamingIntent = new Intent(getBaseContext(), PlayStreamingActivity.class);
                                playStreamingIntent.putExtra(BundleKeys.STREAMING_URL_KEY, event.getStreamingUrl());
                                startActivity(playStreamingIntent);
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        Log.d(TAG, "Playing streaming...");
    }

    private void sendStreamingOKMessage(final Message message) throws SocketException, UnknownHostException, IOException {
        InetAddress ipAddress = InetAddress.getByName(ip);
        byte[] buffer = message.getContent();
        try {
            Socket socket = new Socket(ipAddress, 4448);
            OutputStream outputStream = socket.getOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
            dataOutputStream.writeInt(message.getContentType());
            dataOutputStream.writeInt(buffer.length);
            dataOutputStream.write(buffer, 0, buffer.length);
            socket.close();
        } catch (IOException e) {
            Log.e(TAG, "sendMessage: " + e.getMessage());
        }
    }

    private void addTextMessage(String senderAddress, byte[] messageContent) {
        try {
            Log.d(TAG, "Start addTextMessage");
            String message = new String(messageContent, StandardCharsets.UTF_8);
            message = message.replaceAll("\0+$", "");
            final Message messageItem = new Message(senderAddress, SocketAddressValidator.isLocalhost(senderAddress), MessageType.MESSAGE_TYPE_TEXT, message.getBytes());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.addMessage(messageItem);
                    Log.d(TAG, "End addTextMessage");
                }
            });
        } catch (SocketException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    private void addImageMessage(String senderAddress, byte[] imageBytes) {
        File file = ImageConverter.getImageFileFromBytes(this, imageBytes);
        try {
            final Message messageItem = new Message(senderAddress,
                    SocketAddressValidator.isLocalhost(senderAddress),
                    false, MessageType.MESSAGE_TYPE_IMAGE, file);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.addMessage(messageItem);
                }
            });
        } catch (SocketException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    private void addLocationMessage(String senderAddress, byte[] locationBytes) {
        try {
            final Message messageItem = new Message(senderAddress,
                    SocketAddressValidator.isLocalhost(senderAddress),
                    false, MessageType.MESSAGE_TYPE_LOCATION, locationBytes);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.addMessage(messageItem);
                }
            });
        } catch (SocketException e) {
            Log.e(TAG, e.getMessage());
        }
    }
}
