package vjgarciag96.chatrcmm.ui.streaming;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.Frame;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ShortBuffer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import vjgarciag96.chatrcmm.R;
import vjgarciag96.chatrcmm.common.BundleKeys;
import vjgarciag96.chatrcmm.common.MessageType;
import vjgarciag96.chatrcmm.data.event.StreamingOkEvent;
import vjgarciag96.chatrcmm.data.model.Message;


public class PublishStreamingActivity extends AppCompatActivity implements CameraViewCallback {

    private final static String TAG = "StreamingActivity";

    private PowerManager.WakeLock mWakeLock;

    private String ffmpeg_link = "192.168.1.132";
    private String ip = "";

    private FFmpegFrameRecorder recorder;

    boolean recording = false;
    long startTime = 0;

    private int sampleAudioRateInHz = 44100;
    private int imageWidth = 320;
    private int imageHeight = 240;
    private int frameRate = 30;

    private Thread audioThread;
    volatile boolean runAudioThread = true;

    private AudioRecord audioRecord;
    private AudioRecordRunnable audioRecordRunnable;

    private Frame yuvImage = null;

    @BindView(R.id.start_publishing_button)
    ImageView startPublishingButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_streaming);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            ffmpeg_link = extras.getString(BundleKeys.STREAMING_URL_KEY);
            ip = extras.getString(BundleKeys.TARGET_IP_ADDRESS_KEY);
        }
        ButterKnife.bind(this);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        initViews();
        initRecorder();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mWakeLock == null) {
            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, TAG);
            mWakeLock.acquire();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mWakeLock != null) {
            mWakeLock.release();
            mWakeLock = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        recording = false;
    }

    private void initViews() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Emitiendo en directo");
    }

    @OnClick(R.id.start_publishing_button)
    public void onStartStreamingButtonClicked() {
        if (!recording) {
            try {
                sendPublishStreamingRequest(new Message("Yo", true, false, MessageType.MESSAGE_TYPE_STREAMING_REQUEST, ffmpeg_link.getBytes("UTF-8")));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            startPublishingButton.setImageResource(R.drawable.ic_start);
            stopRecording();
            initRecorder();
        }
    }

    private void sendPublishStreamingRequest(final Message message) {
        try {
            InetAddress ipAddress = InetAddress.getByName(ip);
            byte[] buffer = message.getContent();
            Socket socket = new Socket(ipAddress, 4448);
            OutputStream outputStream = socket.getOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
            dataOutputStream.writeInt(message.getContentType());
            dataOutputStream.writeInt(buffer.length);
            dataOutputStream.write(buffer, 0, buffer.length);
            socket.close();
        } catch (IOException e) {
            Log.e(TAG, "sendMessage: " + e.getMessage());
        }
    }


    private void initRecorder() {

        if (yuvImage == null) {
            yuvImage = new Frame(imageWidth, imageHeight, Frame.DEPTH_UBYTE, 2);
            Log.i(TAG, "create yuvImage");
        }

        Log.i(TAG, "ffmpeg_url: " + ffmpeg_link);
        recorder = new FFmpegFrameRecorder(ffmpeg_link, imageWidth, imageHeight, 1);
        recorder.setFormat("flv");
        recorder.setSampleRate(sampleAudioRateInHz);
        // Set in the surface changed method
        recorder.setFrameRate(frameRate);

        audioRecordRunnable = new AudioRecordRunnable();
        audioThread = new Thread(audioRecordRunnable);
        runAudioThread = true;
    }

    public void startRecording() {
        initRecorder();
        try {
            recorder.start();
            startTime = System.currentTimeMillis();
            recording = true;
            audioThread.start();

        } catch (FFmpegFrameRecorder.Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public void stopRecording() {

        runAudioThread = false;
        try {
            audioThread.join();
        } catch (InterruptedException e) {
            // reset interrupt to be nice
            Thread.currentThread().interrupt();
            return;
        }
        audioRecordRunnable = null;
        audioThread = null;

        if (recorder != null && recording) {
            recording = false;
            Log.v(TAG, "Finishing recording, calling stop and release on recorder");
            try {
                recorder.stop();
                recorder.release();
            } catch (FFmpegFrameRecorder.Exception e) {
                e.printStackTrace();
            }
            recorder = null;

        }
    }

    @Override
    public void onBackPressed() {
        if (recording)
            stopRecording();
        super.onBackPressed();
    }

    @Override
    public void onSurfaceCreated(int width, int height, int frameRate) {
        imageWidth = width;
        imageHeight = height;
        this.frameRate = frameRate;
    }

    @Override
    public void onSurfaceChanged(Frame yuvImage) {
        this.yuvImage = yuvImage;
    }

    @Override
    public void onPreviewFrame(byte[] data) {
        if (audioRecord == null || audioRecord.getRecordingState() != AudioRecord.RECORDSTATE_RECORDING) {
            startTime = System.currentTimeMillis();
            return;
        }


            /* get video data */
        if (yuvImage != null && recording) {
            ((ByteBuffer) yuvImage.image[0].position(0)).put(data);

            long t = 1000 * (System.currentTimeMillis() - startTime);
            if (t > recorder.getTimestamp()) {
                recorder.setTimestamp(t);
            }
            try {
                recorder.record(yuvImage);
            } catch (FFmpegFrameRecorder.Exception e) {
                Log.v(TAG, e.getMessage());
            }
        }
    }

    class AudioRecordRunnable implements Runnable {

        @Override
        public void run() {
            // Set the thread priority
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);

            // Audio
            int bufferSize;
            ShortBuffer audioData;
            int bufferReadResult;

            bufferSize = AudioRecord.getMinBufferSize(sampleAudioRateInHz,
                    AudioFormat.CHANNEL_CONFIGURATION_MONO, AudioFormat.ENCODING_PCM_16BIT);
            audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, sampleAudioRateInHz,
                    AudioFormat.CHANNEL_CONFIGURATION_MONO, AudioFormat.ENCODING_PCM_16BIT, bufferSize);

            audioData = ShortBuffer.allocate(bufferSize);

            Log.d(TAG, "audioRecord.startRecording()");
            audioRecord.startRecording();

            // Audio Capture/Encoding Loop
            while (runAudioThread) {
                bufferReadResult = audioRecord.read(audioData.array(), 0, audioData.capacity());
                audioData.limit(bufferReadResult);
                if (bufferReadResult > 0) {
                    Log.v(TAG, "bufferReadResult: " + bufferReadResult);
                    if (recording) {
                        try {
                            recorder.recordSamples(audioData);
                            //Log.v(LOG_TAG,"recording " + 1024*i + " to " + 1024*i+1024);
                        } catch (FFmpegFrameRecorder.Exception e) {
                            Log.v(TAG, e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }
            }
            Log.v(TAG, "AudioThread Finished");

            /* Capture/Encoding finished, release recorder */
            if (audioRecord != null) {
                audioRecord.stop();
                audioRecord.release();
                audioRecord = null;
                Log.v(TAG, "audioRecord released");
            }
        }
    }


    @Subscribe
    public void onStreamingOk(StreamingOkEvent event) {
        Log.d(TAG, "onStreamingOk: ");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                startPublishingButton.setImageResource(R.drawable.ic_stop);
            }
        });
        startRecording();
    }

}
