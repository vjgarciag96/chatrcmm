package vjgarciag96.chatrcmm.ui.streaming;

import android.content.Context;
import android.hardware.Camera;
import android.media.AudioRecord;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.googlecode.javacv.cpp.opencv_core;

import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.Frame;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.googlecode.javacv.cpp.opencv_core.IPL_DEPTH_8U;


/**
 * Created by victor on 31/03/18.
 */

public class CameraView extends SurfaceView implements SurfaceHolder.Callback, Camera.PreviewCallback {

    private static final String TAG = "CameraView";

    private SurfaceHolder mHolder;
    private Camera mCamera;

    public CameraView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Log.w("camera", "camera view");
        mHolder = getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mCamera = Camera.open();
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.setPreviewCallback(this);

            Camera.Parameters currentParams = mCamera.getParameters();
            Log.v(TAG, "Preview Framerate: " + currentParams.getPreviewFrameRate());
            Log.v(TAG, "Preview imageWidth: " + currentParams.getPreviewSize().width + " imageHeight: " + currentParams.getPreviewSize().height);

            // Use these values
            int imageWidth = currentParams.getPreviewSize().width;
            int imageHeight = currentParams.getPreviewSize().height;
            int frameRate = currentParams.getPreviewFrameRate();
            ((CameraViewCallback)getContext()).onSurfaceCreated(imageWidth, imageHeight, frameRate);
            mCamera.startPreview();
        } catch (IOException exception) {
            mCamera.release();
            mCamera = null;
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // Get the current parameters
        Camera.Parameters currentParams = mCamera.getParameters();
        Log.v(TAG, "Preview Framerate: " + currentParams.getPreviewFrameRate());
        Log.v(TAG, "Preview imageWidth: " + currentParams.getPreviewSize().width + " imageHeight: " + currentParams.getPreviewSize().height);

        // Use these values
        int imageWidth = currentParams.getPreviewSize().width;
        int imageHeight = currentParams.getPreviewSize().height;
        ((CameraViewCallback)getContext()).onSurfaceChanged(new Frame(imageWidth, imageHeight, Frame.DEPTH_UBYTE, 2));
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        try {
            mCamera.setPreviewCallback(null);
            mHolder.addCallback(null);
            mCamera.release();
        } catch (RuntimeException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        ((CameraViewCallback)getContext()).onPreviewFrame(data);
    }
}
