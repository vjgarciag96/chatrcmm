package vjgarciag96.chatrcmm.ui.streaming;


import com.googlecode.javacv.cpp.opencv_core;

import org.bytedeco.javacv.Frame;

/**
 * Created by victor on 31/03/18.
 */

public interface CameraViewCallback {
    void onSurfaceCreated(int width, int height, int frameRate);
    void onSurfaceChanged(Frame yuvImage);
    void onPreviewFrame(byte[] data);
}
